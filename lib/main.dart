import 'package:flutter/material.dart';
import 'package:notes_v2/addnote.dart';
import 'home.dart';
import 'note.dart';
import 'notedit.dart';

void main() {
  runApp(const Notes());
}

class Notes extends StatelessWidget {
  const Notes({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Notes App',
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: const Color.fromRGBO(7, 7, 7, 1),
      ),
      initialRoute: '/home',
      routes: {
        '/home': (context) => const Home(title: ''),
        '/note': (context) => const Note(
              title: '',
              content: '',
            ),
        '/edit': (context) => NoteEdit(
              title: '',
              content: '',
              onSave: (editedNote) {
                Navigator.pop(context, editedNote);
              },
            ),
        '/add': (context) => const NewNote(title: ''),
      },
      onGenerateRoute: (settings) {
        if (settings.name == '/note') {
          final args = settings.arguments as NoteArguments;
          return MaterialPageRoute(
            builder: (context) => Note(
              title: args.title,
              content: args.content,
            ),
          );
        }
        return null;
      },
    );
  }
}

class NoteArguments {
  final String title;
  final String content;

  NoteArguments({required this.title, required this.content});
}
