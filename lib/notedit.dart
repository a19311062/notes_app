import 'package:flutter/material.dart';
import 'package:notes_v2/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class NoteEdit extends StatefulWidget {
  final String title;
  final String content;
  final Function(String) onSave;

  const NoteEdit({
    Key? key,
    required this.title,
    required this.content,
    required this.onSave,
  }) : super(key: key);

  @override
  State<NoteEdit> createState() => _NoteEditState();
}

class _NoteEditState extends State<NoteEdit> {
  late TextEditingController _textEditingController;
  String headerLevel = '';
  String formatText = '';
// Declarar una lista para almacenar el historial de cambios
  List<String> textHistory = []; // Historial de textos
  int currentPosition = 0; // Posición actual en el historial
  int _listNumber = 1;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(text: widget.content);
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> saveNote() async {
    final directory = await getExternalStorageDirectory();
    final notesDirectory = Directory('${directory?.path}/notes');
    if (!await notesDirectory.exists()) {
      await notesDirectory.create(recursive: true);
    }
    final file = File('${notesDirectory.path}/${widget.title}.md');
    await file.writeAsString(_textEditingController.text);

    // Pasar los datos actualizados como resultado al volver a la pantalla Note
    Navigator.pop(context, _textEditingController.text);
  }

  void handleTextChange(String value) {
    if (currentPosition < textHistory.length - 1) {
      textHistory = textHistory.sublist(0, currentPosition + 1);
    }

    textHistory.add(value); // Agregar el nuevo estado del texto al historial
    currentPosition++; // Actualizar la posición actual en el historial

    // Actualizar el texto del controlador solo si es necesario
    if (_textEditingController.text != value) {
      _textEditingController.text = value;
    }
  }

  void undo() {
    if (currentPosition > 0) {
      currentPosition--;
      _textEditingController.text = textHistory[currentPosition];
    }
  }

  void redo() {
    if (currentPosition < textHistory.length - 1) {
      currentPosition++;
      _textEditingController.text = textHistory[currentPosition];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(10, 68, 0, 20),
                    child: Text(
                      widget.title,
                      style: const TextStyle(fontSize: 37),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(
              width: 380,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(255, 21, 168, 0.9),
                    blurRadius: 7,
                    spreadRadius: 1,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 4, 16, 16),
                  child: TextField(
                    cursorColor: const Color(0xFFba44ce),
                    controller: _textEditingController,
                    scrollController: _scrollController,
                    style: const TextStyle(fontSize: 14),
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                    ),
                    onChanged:
                        handleTextChange, // Llamar a la función handleTextChange,
                  ),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: ObsidianBar(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(0.0),
                topRight: Radius.circular(0.0),
              ),
              leadingChild: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.arrow_back_ios_new_rounded,
                  color: Colors.white,
                  size: 30.0,
                ),
              ),
              children: [
                PopupMenuButton<String>(
                  // Tipos de listas
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'bullet',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_list_bulleted,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Bullet list',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'number',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_list_numbered,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Numbered list',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      if (value == 'number') {
                        final currentText = _textEditingController.text;
                        final newText = '$currentText\n$_listNumber. ';
                        _textEditingController.text = newText;
                        _listNumber++;

                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          // Scroll al final del TextField
                          _scrollController.jumpTo(
                              _scrollController.position.maxScrollExtent);
                        });
                      } else if (value == 'bullet') {
                        _textEditingController.text += '\n- ';
                      }
                    });
                  },
                  icon: const Icon(
                    Icons.view_list_sharp,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                PopupMenuButton<String>(
                  // Formato de texto
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'bold',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_bold,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Bold',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'grated',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_underline,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Grated',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'italic',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_italic,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Italic',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      formatText = value;
                      String typeText = '';

                      if (formatText == 'bold') {
                        typeText = '**';
                      } else if (formatText == 'grated') {
                        typeText = '~~';
                      } else if (formatText == 'italic') {
                        typeText = '*';
                      }

                      final String selectedText = _textEditingController
                          .selection
                          .textInside(_textEditingController.text);
                      final int selectionStart =
                          _textEditingController.selection.start;
                      final int selectionEnd =
                          _textEditingController.selection.end;
                      final String newText =
                          _textEditingController.text.replaceRange(
                        selectionStart,
                        selectionEnd,
                        '$typeText$selectedText$typeText',
                      );

                      final int newCursorPosition =
                          selectionStart + typeText.length;
                      _textEditingController.value =
                          _textEditingController.value.copyWith(
                        text: newText,
                        selection:
                            TextSelection.collapsed(offset: newCursorPosition),
                      );
                    });
                  },
                  icon: const Icon(
                    Icons.text_format,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                PopupMenuButton<String>(
                  //Titulos
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'h1',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Jumbo',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h2',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Big',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h3',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Medium',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h4',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Small',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h5',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Little',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      headerLevel = value;
                      String headerText = '';

                      if (headerLevel == 'h1') {
                        headerText = '# ';
                      } else if (headerLevel == 'h2') {
                        headerText = '## ';
                      } else if (headerLevel == 'h3') {
                        headerText = '### ';
                      } else if (headerLevel == 'h4') {
                        headerText = '#### ';
                      } else if (headerLevel == 'h5') {
                        headerText = '##### ';
                      }

                      final int currentPosition =
                          _textEditingController.selection.base.offset;
                      final String newText =
                          _textEditingController.text.replaceRange(
                        currentPosition,
                        currentPosition,
                        '$headerText',
                      );
                      _textEditingController.value =
                          _textEditingController.value.copyWith(
                        text: newText,
                        selection: TextSelection.fromPosition(
                          TextPosition(
                              offset: currentPosition + headerText.length),
                        ),
                      );
                    });
                  },
                  icon: const Icon(
                    Icons.text_fields_outlined,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: saveNote,
                  icon: const Icon(
                    Icons.save,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: undo,
                  icon: const Icon(
                    Icons.undo,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: redo,
                  icon: const Icon(
                    Icons.redo,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
