import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'notedit.dart';
import 'widgets.dart';

class Note extends StatefulWidget {
  final String title;
  final String content;

  const Note({
    Key? key,
    required this.title,
    required this.content,
  }) : super(key: key);

  @override
  State<Note> createState() => _NoteState();
}

class _NoteState extends State<Note> {
  late String _content;

  @override
  void initState() {
    super.initState();
    _content = widget.content;
  }

  void updateContent(String newContent) {
    setState(() {
      _content = newContent;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(10, 68, 0, 20),
                    child: Text(
                      widget.title,
                      style: const TextStyle(fontSize: 37),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(
              width: 380,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(255, 21, 168, 0.9),
                    blurRadius: 7,
                    spreadRadius: 1,
                    offset: Offset(0, 0),
                  ),
                ],
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(0, 0, 0, 1),
                    Color.fromRGBO(0, 0, 0, 1),
                    Color.fromRGBO(0, 0, 0, 1),
                  ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: MarkdownBody(
                    data: _content,
                    styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context)),
                    onTapLink: (text, href, title) {
                      // Manejar el enlace tocado (opcional)
                    },
                  ),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: ObsidianBar(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(0.0),
                topRight: Radius.circular(0.0),
              ),
              leadingChild: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/home');
                },
                icon: const Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.white,
                  size: 30.0,
                ),
              ),
              children: [
                IconButton(
                  onPressed: () async {
                    final editedNote = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NoteEdit(
                          title: widget.title,
                          content: _content,
                          onSave: updateContent,
                        ),
                      ),
                    );
                    if (editedNote != null) {
                      updateContent(editedNote);
                    }
                  },
                  icon: const Icon(
                    Icons.mode_edit,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text(
                            'Pressing this icon shows a menu with the options "rename" or "delete" for example.'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                  },
                  icon: const Icon(
                    Icons.more_vert,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
