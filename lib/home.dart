import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'addnote.dart';
import 'note.dart';
import 'widgets.dart';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

enum GridViewMode { Single, Double, Triple, Row }

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<NoteAdd> notes = [];
  GridViewMode viewMode = GridViewMode.Double;

  String searchQuery = '';
  List<NoteAdd> filteredNotes = [];

  @override
  void initState() {
    super.initState();
    loadNotes();
    loadViewMode();
  }

  Future<void> loadNotes() async {
    final directory = await getExternalStorageDirectory();
    final notesDirectory = Directory('${directory?.path}/notes');
    final files = await notesDirectory.list().toList();

    notes.clear();
    filteredNotes.clear();

    await Future.wait(files.map((file) async {
      final content = await File(file.path).readAsString();
      final title = path.basenameWithoutExtension(file.path);
      if (title.isNotEmpty && content.isNotEmpty) {
        final note = NoteAdd(title: title, content: content);
        notes.add(note);
        filteredNotes.add(note);
      }
    }));

    setState(() {});
  }

  Future<void> loadViewMode() async {
    final preferences = await SharedPreferences.getInstance();
    final mode = preferences.getInt('viewMode');

    if (mode != null) {
      setState(() {
        viewMode = GridViewMode.values[mode];
      });
    }
  }

  Future<void> saveViewMode() async {
    final preferences = await SharedPreferences.getInstance();
    preferences.setInt('viewMode', viewMode.index);
  }

  Future<void> refreshNotes() async {
    setState(() {
      filteredNotes =
          List.from(notes); // Actualizar la lista de notas filtradas
    });
  }

  Widget buildNoteCard(NoteAdd note) {
    int maxLines = 2;
    double titleFontSize = 25;
    double contentFontSize = 18;

    if (viewMode == GridViewMode.Single) {
      maxLines = 16;
    } else if (viewMode == GridViewMode.Double) {
      maxLines = 6;
      titleFontSize = 20;
      contentFontSize = 16;
    } else if (viewMode == GridViewMode.Triple) {
      maxLines = 3;
      titleFontSize = 16;
      contentFontSize = 13;
    }

    return NoteCard(
      title: note.title,
      content: note.content,
      maxLines: maxLines,
      titleFontSize: titleFontSize,
      contentFontSize: contentFontSize, //  el tamaño de fuente del contenido
    );
  }

  Widget buildNoteListView(NoteAdd note, int noteIndex) {
    return Builder(
      builder: (context) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Note(
                  title: note.title,
                  content: note.content,
                ),
              ),
            );
          },
          onLongPress: () {
            _showPopupMenu(context, note, noteIndex);
          },
          child: Container(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  note.title,
                  style: const TextStyle(
                    fontSize: 20,
                  ),
                ),
                const SizedBox(height: 8.0),
                Text(
                  note.content,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Color.fromRGBO(135, 135, 135, 1),
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _showPopupMenu(BuildContext context, NoteAdd note, int noteIndex) async {
    final RenderBox titleBox = context.findRenderObject() as RenderBox;
    final Offset titlePosition = titleBox.localToGlobal(Offset.zero);

    final result = await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(
        titlePosition.dx + titleBox.size.width,
        titlePosition.dy,
        titlePosition.dx + (2 * titleBox.size.width),
        titlePosition.dy + titleBox.size.height,
      ),
      items: [
        const PopupMenuItem<String>(
          value: 'rename',
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.edit,
                    size: 18,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Rename',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Divider(
                color: Color.fromRGBO(255, 21, 168, 1),
                thickness: 1.0,
                height: 1.0,
              ),
            ],
          ),
        ),
        const PopupMenuItem<String>(
          value: 'delete',
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.delete_rounded,
                    size: 18,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Delete',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Divider(
                color: Color.fromRGBO(255, 21, 168, 1),
                thickness: 1.0,
                height: 1.0,
              ),
            ],
          ),
        ),
      ],
      color: const Color(0xFF222222),
    );

    if (result == 'delete') {
      final directory = await getExternalStorageDirectory();
      final noteDirectory = Directory('${directory?.path}/notes');
      final filePath = path.join(noteDirectory.path, '${note.title}.md');

      await File(filePath).delete();

      Navigator.pop(context);
      Navigator.pushReplacementNamed(context, '/home');
    }

    if (result == 'rename') {
      // ignore: use_build_context_synchronously
      final editedTitle = await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          String newTitle = note.title;

          return AlertDialog(
            backgroundColor: const Color(0xFF222222),
            title: const Text(
              'Rename',
              style: TextStyle(fontSize: 25),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Divider(
                  color: Color.fromRGBO(255, 21, 168, 1),
                  thickness: 1.0,
                  height: 1.0,
                ),
                TextField(
                  controller: TextEditingController(text: note.title),
                  style: const TextStyle(fontSize: 18),
                  decoration: const InputDecoration(
                    hintText: 'Enter title',
                    hintMaxLines: null,
                    hintStyle: TextStyle(fontSize: 20),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(12.0),
                  ),
                  textCapitalization: TextCapitalization.sentences,
                  onChanged: (value) {
                    newTitle = value;
                  },
                ),
              ],
            ),
            actions: [
              ButtonNeon(
                onPressed: () {
                  Navigator.pop(context);
                },
                text: 'Cancel',
              ),
              ButtonNeon(
                onPressed: () async {
                  if (newTitle.trim().isNotEmpty) {
                    final trimmedTitle = newTitle.trim();

                    final directory = await getExternalStorageDirectory();
                    final noteDirectory = Directory('${directory?.path}/notes');
                    final oldFilePath =
                        path.join(noteDirectory.path, '${note.title}.md');
                    final newFilePath =
                        path.join(noteDirectory.path, '$trimmedTitle.md');

                    await File(oldFilePath).rename(newFilePath);

                    Navigator.pop(context, trimmedTitle);
                    Navigator.pushReplacementNamed(context, '/home');
                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text(
                          'Error',
                          style: TextStyle(fontSize: 25),
                        ),
                        backgroundColor: const Color(0xFF222222),
                        content: const Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Divider(
                              color: Color.fromRGBO(255, 21, 168, 1),
                              thickness: 1.0,
                              height: 1.0,
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Please enter a valid title.',
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                        actions: [
                          ButtonNeon(
                            text: ('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    );
                  }
                },
                text: 'Save',
              ),
            ],
          );
        },
      );

      if (editedTitle != null && editedTitle.isNotEmpty) {
        final trimmedTitle = editedTitle.trim();

        final directory = await getExternalStorageDirectory();
        final noteDirectory = Directory('${directory?.path}/notes');
        final oldFilePath = path.join(noteDirectory.path, '${note.title}.md');
        final newFilePath = path.join(noteDirectory.path, '$trimmedTitle.md');

        await File(oldFilePath).rename(newFilePath);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const TitleHeader(
            flex: 3,
            title: 'Every notes',
            imagePath: 'assets/picture.png',
          ),
          const Divider(
            color: Color.fromRGBO(255, 21, 168, 1),
            thickness: 1.0,
            height: 1.0,
          ),
          Flexible(
            flex: 11,
            child: RefreshIndicator(
              onRefresh: refreshNotes,
              child: Visibility(
                visible: filteredNotes
                    .isEmpty, // Cambiar `notes.isEmpty` por `filteredNotes.isEmpty`
                replacement: viewMode == GridViewMode.Row
                    ? ListView.builder(
                        itemCount: filteredNotes
                            .length, // Cambiar `notes.length` por `filteredNotes.length`
                        itemBuilder: (context, index) {
                          final note = filteredNotes[
                              index]; // Cambiar `notes[index]` por `filteredNotes[index]`
                          return buildNoteListView(note, index);
                        },
                      )
                    : GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: viewMode == GridViewMode.Single
                              ? 1
                              : viewMode == GridViewMode.Double
                                  ? 2
                                  : viewMode == GridViewMode.Triple
                                      ? 3
                                      : 1,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 0,
                        ),
                        itemCount: filteredNotes
                            .length, // Cambiar `notes.length` por `filteredNotes.length`
                        itemBuilder: (context, index) {
                          return buildNoteCard(filteredNotes[
                              index]); // Cambiar `notes[index]` por `filteredNotes[index]`
                        },
                      ),
                child: const Center(
                  child: Text(
                    'There are currently no notes to view, create one! :D',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: ObsidianBar(
              textField: TextField(
                cursorColor: const Color(0xFFba44ce),
                style: const TextStyle(
                  fontSize: 25,
                ),
                decoration: const InputDecoration(
                  hintText: 'Search note',
                  hintStyle: TextStyle(
                    fontSize: 25,
                    color: Color.fromRGBO(122, 122, 122, 1),
                  ),
                  border: InputBorder.none,
                ),
                onChanged: (value) {
                  searchNotes(value);
                },
              ),
              children: [
                IconButton(
                  onPressed: () async {
                    final result =
                        await Navigator.pushNamed(context, '/add') as NoteAdd?;
                    if (result != null) {
                      setState(() {
                        notes.add(result);
                      });
                    }
                  },
                  icon: const Icon(
                    Icons.add_box,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      if (viewMode == GridViewMode.Single) {
                        viewMode = GridViewMode.Double;
                      } else if (viewMode == GridViewMode.Double) {
                        viewMode = GridViewMode.Triple;
                      } else if (viewMode == GridViewMode.Triple) {
                        viewMode = GridViewMode.Row;
                      } else if (viewMode == GridViewMode.Row) {
                        viewMode = GridViewMode.Single;
                      }
                      saveViewMode();
                    });
                  },
                  icon: Icon(
                    viewMode == GridViewMode.Single
                        ? Icons.crop_square_rounded
                        : viewMode == GridViewMode.Double
                            ? Icons.grid_view_rounded
                            : viewMode == GridViewMode.Triple
                                ? Icons.view_column_rounded
                                : Icons.view_agenda_rounded,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void searchNotes(String query) {
    setState(() {
      if (query.isNotEmpty) {
        filteredNotes = notes.where((note) {
          final noteTitle = note.title.toLowerCase();
          final noteContent = note.content.toLowerCase();
          final searchKeywords = query.toLowerCase().split(' ');

          // Buscar en el título y contenido de la nota
          for (final keyword in searchKeywords) {
            if (!(noteTitle.contains(keyword) ||
                noteContent.contains(keyword))) {
              return false;
            }
          }
          return true;
        }).toList();
      } else {
        filteredNotes = List.from(notes);
      }
    });
  }
}
