import 'package:flutter/material.dart';
import 'widgets.dart';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class NoteAdd {
  final String title;
  final String content;

  NoteAdd({required this.title, required this.content});
}

class NewNote extends StatefulWidget {
  const NewNote({Key? key, required String title}) : super(key: key);

  @override
  _NewNoteState createState() => _NewNoteState();
}

class _NewNoteState extends State<NewNote> {
  late TextEditingController _titleController;
  late TextEditingController _contentController;
  int currentPosition = 0;
  List<String> textHistory = [];
  String headerLevel = '';
  String formatText = '';

  int _numberedListCount = 1;

  String _getNumberedList() {
    String numberedList = '$_numberedListCount.-';
    _numberedListCount++;
    return numberedList;
  }

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _contentController = TextEditingController();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _contentController.dispose();
    super.dispose();
  }

  Future<void> _saveNote() async {
    final String title = _titleController.text.trim();
    final String content = _contentController.text.trim();

    if (content.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Note cannot be empty'),
          duration: Duration(seconds: 2),
        ),
      );
      return;
    }

    final Noted note = Noted(
      title: title.isNotEmpty ? title : 'New note',
      content: content,
    );

    await _saveNoteAsMarkdown(note);

    Navigator.pushReplacementNamed(context, '/home');
  }

  Future<void> _saveNoteAsMarkdown(Noted note) async {
    final directory = await getExternalStorageDirectory();
    final notesDirectory = Directory('${directory?.path}/notes');
    await notesDirectory.create(recursive: true);

    String title = note.title;
    String filePath = path.join(notesDirectory.path, '$title.md');
    int counter = 1;

    // Verificar si ya existe una nota con el mismo título
    while (await File(filePath).exists()) {
      // Añadir número entre paréntesis al título
      title = '${note.title} ($counter)';
      filePath = path.join(notesDirectory.path, '$title.md');
      counter++;
    }

    await File(filePath).writeAsString(note.content);
  }

  void handleTextChange(String value) {
    if (currentPosition < textHistory.length - 1) {
      textHistory = textHistory.sublist(0, currentPosition + 1);
    }

    textHistory.add(value);
    currentPosition++;

    if (_contentController.text != value) {
      _contentController.text = value;
    }
  }

  void undo() {
    if (currentPosition > 0) {
      currentPosition--;
      _contentController.text = textHistory[currentPosition];
    }
  }

  void redo() {
    if (currentPosition < textHistory.length - 1) {
      currentPosition++;
      _contentController.text = textHistory[currentPosition];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(10, 58, 10, 20),
                    child: TextFormField(
                      controller: _titleController,
                      decoration: const InputDecoration(
                        hintText: 'Enter title',
                        hintStyle: TextStyle(fontSize: 37),
                        border: InputBorder.none,
                      ),
                      textCapitalization: TextCapitalization.sentences,
                      style: const TextStyle(
                        fontSize: 37,
                      ),
                      maxLines: 1,
                      textInputAction: TextInputAction.done,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(
              margin: const EdgeInsets.fromLTRB(1, 0, 1, 0),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(255, 21, 168, 0.9),
                    blurRadius: 7,
                    spreadRadius: 1,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: TextField(
                cursorColor: const Color(0xFFba44ce),
                controller: _contentController,
                maxLines: null,
                style: const TextStyle(fontSize: 18),
                decoration: const InputDecoration(
                  hintText: 'Enter content...',
                  hintMaxLines: null,
                  hintStyle: TextStyle(fontSize: 14),
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(12.0),
                ),
                onChanged: handleTextChange,
              ),
            ),
          ),
          SingleChildScrollView(
            child: ObsidianBar(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(0.0),
                topRight: Radius.circular(0.0),
              ),
              leadingChild: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.arrow_back_ios_new_rounded,
                  color: Colors.white,
                  size: 30.0,
                ),
              ),
              children: [
                PopupMenuButton<String>(
                  // Tipos de listas
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'number',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_list_numbered,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Numbered list',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'bullet',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_list_bulleted,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Bullet list',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'item',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.list_sharp,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Item list',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      if (value == 'number') {
                        _contentController.text += '${_getNumberedList()} ';
                      } else if (value == 'bullet') {
                        _contentController.text += '• ';
                      } else if (value == 'item') {
                        _contentController.text += '- ';
                      }
                    });
                  },
                  icon: const Icon(
                    Icons.view_list_sharp,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                PopupMenuButton<String>(
                  // Formato de texto
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'bold',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_bold,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Bold',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'grated',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_underline,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Grated',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'italic',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.format_italic,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Italic',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      formatText = value;
                      String typeText = '';

                      if (formatText == 'bold') {
                        typeText = '**';
                      } else if (formatText == 'grated') {
                        typeText = '~~';
                      } else if (formatText == 'italic') {
                        typeText = '*';
                      }

                      final String selectedText = _contentController.selection
                          .textInside(_contentController.text);
                      final int selectionStart =
                          _contentController.selection.start;
                      final int selectionEnd = _contentController.selection.end;
                      final String newText =
                          _contentController.text.replaceRange(
                        selectionStart,
                        selectionEnd,
                        '$typeText$selectedText$typeText',
                      );

                      final int newCursorPosition =
                          selectionStart + typeText.length;
                      _contentController.value =
                          _contentController.value.copyWith(
                        text: newText,
                        selection:
                            TextSelection.collapsed(offset: newCursorPosition),
                      );
                    });
                  },
                  icon: const Icon(
                    Icons.text_format,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                PopupMenuButton<String>(
                  //Titulos
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    const PopupMenuItem<String>(
                      value: 'h1',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Jumbo',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h2',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Big',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h3',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Medium',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h4',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Small',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const PopupMenuItem<String>(
                      value: 'h5',
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.h_mobiledata_outlined,
                                size: 25,
                                color: Colors.white,
                              ),
                              SizedBox(width: 8),
                              Text(
                                'Little',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Divider(
                            color: Color.fromRGBO(255, 21, 168, 1),
                            thickness: 1.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                  color: const Color(0xFF222222),
                  onSelected: (String value) {
                    setState(() {
                      headerLevel = value;
                      String headerText = '';

                      if (headerLevel == 'h1') {
                        headerText = '# ';
                      } else if (headerLevel == 'h2') {
                        headerText = '## ';
                      } else if (headerLevel == 'h3') {
                        headerText = '### ';
                      } else if (headerLevel == 'h4') {
                        headerText = '#### ';
                      } else if (headerLevel == 'h5') {
                        headerText = '##### ';
                      }

                      final int currentPosition =
                          _contentController.selection.base.offset;
                      final String newText =
                          _contentController.text.replaceRange(
                        currentPosition,
                        currentPosition,
                        '$headerText',
                      );
                      _contentController.value =
                          _contentController.value.copyWith(
                        text: newText,
                        selection: TextSelection.fromPosition(
                          TextPosition(
                              offset: currentPosition + headerText.length),
                        ),
                      );
                    });
                  },
                  icon: const Icon(
                    Icons.text_fields_outlined,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: _saveNote,
                  icon: const Icon(
                    Icons.save,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: undo,
                  icon: const Icon(
                    Icons.undo,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                IconButton(
                  onPressed: redo,
                  icon: const Icon(
                    Icons.redo,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
