import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'home.dart';
import 'note.dart';

class NoteMini extends StatelessWidget {
  const NoteMini({
    Key? key,
    required this.title,
    required this.subtitle,
    this.onPressed,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Expanded(
        flex: 1,
        child: GestureDetector(
          onTap: onPressed,
          child: Container(
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(68, 68, 68, 1),
              borderRadius: BorderRadius.circular(20),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(233, 255, 9, 0.7),
                  blurRadius: 3,
                  spreadRadius: 0,
                  offset: Offset(0, 3),
                ),
              ],
              gradient: const LinearGradient(
                colors: [
                  Color.fromRGBO(68, 68, 68, 1),
                  Color.fromRGBO(68, 68, 68, 1),
                  Color.fromRGBO(68, 68, 68, 1),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    title,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 25,
                    ),
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(0, 7, 5, 0)),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    subtitle,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ObsidianBar extends StatelessWidget {
  final List<Widget> children;
  final Widget? textField;
  final Widget? leadingChild;
  final BorderRadius borderRadius;

  const ObsidianBar({
    Key? key,
    this.children = const [],
    this.textField,
    this.leadingChild,
    this.borderRadius = const BorderRadius.only(
      topLeft: Radius.circular(20.0),
      topRight: Radius.circular(20.0),
    ),
  })  : assert(children.length <= 10),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(0, 0, 0, 1),
            borderRadius: borderRadius,
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(255, 21, 168, 1),
                blurRadius: 7,
                spreadRadius: -3,
                offset: Offset(0, -5),
              ),
            ],
          ),
          child: Row(
            children: [
              if (leadingChild != null) leadingChild!,
              const Padding(padding: EdgeInsets.fromLTRB(17, 0, 0, 0)),
              Expanded(child: textField ?? Container()),
              ...children,
              const Padding(padding: EdgeInsets.only(right: 15)),
            ],
          ),
        ),
      ),
    );
  }
}

class Notedit extends StatelessWidget {
  final TextEditingController? controller;
  final int? flex;

  const Notedit({
    Key? key,
    this.controller,
    this.flex = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex!,
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromRGBO(78, 78, 78, 1),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(233, 255, 9, 0.7),
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, -2),
            ),
          ],
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(78, 78, 78, 1),
              Color.fromRGBO(78, 78, 78, 1),
              Color.fromRGBO(78, 78, 78, 1),
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: TextField(
          controller: controller,
          maxLines: 25,
          style: const TextStyle(fontSize: 18),
          decoration: const InputDecoration(
            hintMaxLines: 25,
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(16.0),
          ),
        ),
      ),
    );
  }
}

class ViewNote extends StatelessWidget {
  final Widget child;
  final int? flex;

  const ViewNote({
    Key? key,
    required this.child,
    this.flex = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex!,
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromRGBO(78, 78, 78, 1),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(233, 255, 9, 0.7),
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, -2),
            ),
          ],
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(78, 78, 78, 1),
              Color.fromRGBO(78, 78, 78, 1),
              Color.fromRGBO(78, 78, 78, 1),
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: child,
        ),
      ),
    );
  }
}

class TitleHeader extends StatelessWidget {
  final String title;
  final int? flex;
  final String? imagePath; // Nueva propiedad para la ruta de la imagen

  const TitleHeader({
    Key? key,
    required this.title,
    this.flex = 3,
    this.imagePath, // Asignamos la nueva propiedad
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: flex!,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 60, 0, 30),
            child: Row(
              children: [
                Text(
                  title,
                  style: const TextStyle(fontSize: 37),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(150, 0, 0, 30),
                  child: SizedBox(height: 50),
                ),
                if (imagePath != null)
                  Container(
                    width: 40,
                    height: 40,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(107, 98, 105, 1),
                    ),
                    child: ClipOval(
                      child: Image.asset(imagePath!),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Noted {
  final String title;
  final String content;

  Noted({required this.title, required this.content});
}

class ButtonNeon extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;

  const ButtonNeon({
    super.key,
    required this.onPressed,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 40,
      margin: const EdgeInsets.all(15),
      decoration: const BoxDecoration(
        color: Color.fromRGBO(78, 78, 78, 1),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
          bottomLeft: Radius.circular(10.0),
          bottomRight: Radius.circular(10.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(255, 21, 168, 1),
            blurRadius: 5,
            spreadRadius: 1,
            offset: Offset(0, 0),
          ),
        ],
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(0, 0, 0, 1),
            Color.fromRGBO(0, 0, 0, 1),
            Color.fromRGBO(0, 0, 0, 1),
          ],
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
        ),
      ),
      child: MaterialButton(
        onPressed: onPressed,
        child: Text(
          text,
          style: const TextStyle(fontSize: 17),
        ),
      ),
    );
  }
}

class NoteCard extends StatelessWidget {
  final String title;
  final String content;
  final int maxLines;
  final double titleFontSize;
  final double contentFontSize;

  const NoteCard({
    Key? key,
    required this.title,
    required this.content,
    this.maxLines = 2,
    this.titleFontSize = 25,
    this.contentFontSize = 18,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GridViewMode viewMode = GridViewMode.Double;

    return GestureDetector(
      onLongPress: () {
        _showPopupMenu(context);
      },
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Note(
              title: title,
              content: content,
            ),
          ),
        );
      },
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            margin: const EdgeInsets.fromLTRB(2, 0, 8, 10),
            padding: const EdgeInsets.fromLTRB(15, 8, 15, 15),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(34, 34, 34, 1),
              borderRadius: BorderRadius.circular(20),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(255, 21, 168, 1),
                  blurRadius: 5,
                  spreadRadius: -3,
                  offset: Offset(5, 5),
                ),
              ],
              gradient: const LinearGradient(
                colors: [
                  Color.fromRGBO(0, 0, 0, 1),
                  Color.fromRGBO(0, 0, 0, 1),
                  Color.fromRGBO(0, 0, 0, 1),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize:
                        viewMode == GridViewMode.Triple ? 10 : titleFontSize,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  content,
                  style: TextStyle(
                    color: const Color.fromRGBO(135, 135, 135, 1),
                    fontSize:
                        viewMode == GridViewMode.Triple ? 8 : contentFontSize,
                  ),
                  maxLines: maxLines,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _showPopupMenu(BuildContext context) async {
    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;
    final RenderBox cardBox = context.findRenderObject() as RenderBox;
    final Offset cardPosition =
        cardBox.localToGlobal(Offset.zero, ancestor: overlay);

    final result = await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(
        cardPosition.dx + cardBox.size.width,
        cardPosition.dy,
        cardPosition.dx + (2 * cardBox.size.width),
        cardPosition.dy + cardBox.size.height,
      ),
      items: [
        const PopupMenuItem<String>(
          value: 'rename',
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.edit,
                    size: 18,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Rename',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Divider(
                color: Color.fromRGBO(255, 21, 168, 1),
                thickness: 1.0,
                height: 1.0,
              ),
            ],
          ),
        ),
        const PopupMenuItem<String>(
          value: 'delete',
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.delete_rounded,
                    size: 18,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Delete',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Divider(
                color: Color.fromRGBO(255, 21, 168, 1),
                thickness: 1.0,
                height: 1.0,
              ),
            ],
          ),
        ),
      ],
      color: const Color(0xFF222222),
    );

    if (result == 'delete') {
      final directory = await getExternalStorageDirectory();
      final noteDirectory = Directory('${directory?.path}/notes');
      final filePath = path.join(noteDirectory.path, '$title.md');

      await File(filePath).delete();

      Navigator.pop(context);
      Navigator.pushReplacementNamed(context, '/home');
    }

    if (result == 'rename') {
      // ignore: use_build_context_synchronously
      final editedTitle = await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          String newTitle = title;

          return AlertDialog(
            backgroundColor: const Color(0xFF222222),
            title: const Text(
              'Rename',
              style: TextStyle(fontSize: 25),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Divider(
                  color: Color.fromRGBO(255, 21, 168, 1),
                  thickness: 1.0,
                  height: 1.0,
                ),
                TextField(
                  controller: TextEditingController(text: title),
                  style: const TextStyle(fontSize: 18),
                  decoration: const InputDecoration(
                    hintText: 'Enter title',
                    hintMaxLines: null,
                    hintStyle: TextStyle(fontSize: 20),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(12.0),
                  ),
                  textCapitalization: TextCapitalization.sentences,
                  onChanged: (value) {
                    newTitle = value;
                  },
                ),
              ],
            ),
            actions: [
              ButtonNeon(
                onPressed: () {
                  Navigator.pop(context);
                },
                text: 'Cancel',
              ),
              ButtonNeon(
                onPressed: () async {
                  if (newTitle.trim().isNotEmpty) {
                    final trimmedTitle = newTitle
                        .trim(); // Eliminar espacios vacíos al inicio y final

                    final directory = await getExternalStorageDirectory();
                    final noteDirectory = Directory('${directory?.path}/notes');
                    final oldFilePath =
                        path.join(noteDirectory.path, '$title.md');
                    var newFilePath =
                        path.join(noteDirectory.path, '$trimmedTitle.md');

                    // Verificar si ya existe una nota con el nuevo título
                    int counter = 1;
                    while (await File(newFilePath).exists()) {
                      final numberedTitle = '$trimmedTitle ($counter)';
                      newFilePath =
                          path.join(noteDirectory.path, '$numberedTitle.md');
                      counter++;
                    }

                    await File(oldFilePath).rename(newFilePath);

                    Navigator.pop(context, trimmedTitle);
                    Navigator.pushReplacementNamed(context, '/home');
                  } else {
                    // Show an error dialog for an empty title
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text(
                          'Error',
                          style: TextStyle(fontSize: 25),
                        ),
                        backgroundColor: const Color(0xFF222222),
                        content: const Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Divider(
                              color: Color.fromRGBO(255, 21, 168, 1),
                              thickness: 1.0,
                              height: 1.0,
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Please enter a valid title.',
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                        actions: [
                          ButtonNeon(
                            text: ('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    );
                  }
                },
                text: 'Save',
              ),
            ],
          );
        },
      );

      if (editedTitle != null && editedTitle.isNotEmpty) {
        final trimmedTitle =
            editedTitle.trim(); // Eliminar espacios vacíos al inicio y final

        final directory = await getExternalStorageDirectory();
        final noteDirectory = Directory('${directory?.path}/notes');
        final oldFilePath = path.join(noteDirectory.path, '$title.md');
        final newFilePath = path.join(noteDirectory.path, '$trimmedTitle.md');

        await File(oldFilePath).rename(newFilePath);
      }
    }
  }
}
